
# {{title}}

## Описание
{{description}}

## Мета данные

* Сложность: {{complexity}} з 5
* Лимит по времени выполнения: {{timeLimit}} с
* Лимит по занимаемой памяти: {{memoryLimit}} кБ

## Входные/выходные данные

### Входные данные:

{{input.description}}

### Выходные данные

{{output.description}}

### Пример входных/выходных данных

| Входные данные | Выходные данные |
|:-------------:| :-----:|

{{for i = 0; i < input.sample.size(); i++ }}

| {{input.sample[i]}} | {{output.sample[i]}} |

{{end for}}
